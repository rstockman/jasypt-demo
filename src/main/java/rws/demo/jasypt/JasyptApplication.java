package rws.demo.jasypt;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.spring3.properties.EncryptablePropertyPlaceholderConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
@ComponentScan
public class JasyptApplication {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		ApplicationContext context = new AnnotationConfigApplicationContext(JasyptApplication.class);
		Database database = context.getBean(Database.class);

		System.out.println(database);

	}
	
	@Bean
	public EnvironmentStringPBEConfig environmentVariablesConfiguration() {
		EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
		config.setAlgorithm("PBEWITHMD5ANDTRIPLEDES");
		config.setPassword("sw0rdf1sh");
		return config;
	}

	@Bean
	public StandardPBEStringEncryptor configurationEncryptor(EnvironmentStringPBEConfig config) {
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		encryptor.setConfig(config);
		return encryptor;
	}

	@Bean
	public EncryptablePropertyPlaceholderConfigurer placeholderProperties(StandardPBEStringEncryptor encryptor) {
		EncryptablePropertyPlaceholderConfigurer placeholders = new EncryptablePropertyPlaceholderConfigurer(
				encryptor);
		placeholders.setIgnoreResourceNotFound(true);
		placeholders.setSystemPropertiesModeName("SYSTEM_PROPERTIES_MODE_OVERRIDE");
		placeholders.setLocations(new Resource[] { new ClassPathResource("application.properties") });
		return placeholders;
	}
}
