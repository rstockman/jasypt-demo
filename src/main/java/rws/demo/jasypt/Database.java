package rws.demo.jasypt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Database {

	@Value("${db.username}")
	private String username;

	@Value("${db.password.unencrypted}")
	private String unencryptedPassword;

	@Value("${db.password.encrypted}")
	private String encryptedPassword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUnencryptedPassword() {
		return unencryptedPassword;
	}

	public void setUnencryptedPassword(String unencryptedPassword) {
		this.unencryptedPassword = unencryptedPassword;
	}

	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	@Override
	public String toString() {
		return "username = " + username + "\nunencryptedPassword = " + unencryptedPassword + "\nencryptedPassword = "
				+ encryptedPassword;
	}
}